//requirement packets
let chai = require('chai');
let chaiHttp = require('chai-http');
let assertArrays = require('chai-arrays');
let chaiSubset = require('chai-subset');
const expect = require('chai').expect;
chai.use(chaiSubset);
chai.use(assertArrays);
chai.use(chaiHttp);
//url to call
const urlBase= 'http://192.168.100.26:5001/api/v1';
var token= "";

describe('Login Swagger:',()=>{
			it('Expected response "User data": Status 200', (done) => {
							chai.request(urlBase)
								.post('/users/login')
								.set('content-type', 'application/x-www-form-urlencoded')
								.send({email:'qaebike@gmail.com', password: 'Ebike-app' })
                .set('Accept-Language', 'es_ES')
								//.set('accept-encoding', 'application/json')
								.end( function(err,res){
									expect(res.body.status).to.equal(200);
                      token = res.body.data[0].token;
									done();
								});
				});
      it('All brands', (done) => {
           chai.request(urlBase)
               .get('/brands')
               .set('content-type', 'application/x-www-form-urlencoded')
               .set('Authorization', 'Bearer ' + token)
               .set('accept-encoding', 'application/json')
               .set('Accept-Language', 'es_ES')
               .end( function(err,res){
                 expect(res.body.status).to.equal(200);
                   done();               });

        });
    });
