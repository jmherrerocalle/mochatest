//paquetes que requerimos
let chai = require('chai');
let chaiHttp = require('chai-http');
let assertArrays = require('chai-arrays');
let chaiSubset = require('chai-subset');
const expect = require('chai').expect;
chai.use(chaiSubset);
chai.use(assertArrays);
chai.use(chaiHttp);
//definimos también la Url a la que vamos a llamar.
const urlBase= 'http://192.168.100.26:5001/api/v1';
var Cookies; //Cookie to auth
var Token; //Token to auth

describe('UC-1 postLogin Base:',()=>{
		it('Valid Email and password. Status 200', (done) => {
					chai.request(urlBase)
						.post('/users/login')
						.set('content-type', 'application/x-www-form-urlencoded')
						.send({email:'qaebike@gmail.com', password: 'Ebike-app' })
						.end( function(err,res){
							expect(res).to.have.status('200');
							expect(res.body.status).to.equal(res.status);
							expect('set-cookie', 'cookie=hey; Path=/', done);
							Cookies = res.headers['set-cookie'].pop().split(';')[0];
							Token = res.body.data[0].token;
							done();
						});
			});
			it('Expected response "User data": Status 200', (done) => {
							chai.request(urlBase)
								.post('/users/login')
								.set('content-type', 'application/x-www-form-urlencoded')
								.send({email:'qaebike@gmail.com', password: 'Ebike-app' })
								.end( function(err,res){
									expect(res).to.have.status('200');
									expect(res.body.status).to.equal(res.status);
									expect()
									expect(res.body.data).containSubset([{
										id: 8946,
										public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
										parent_users_id: null,
										firstname: 'qa',
										surname: 'YasminaProducción',
										birthdate: '2019-01-21 00:00:00',
										height: 159,
										weight: 56,
										id_facebook: null,
										gender: 'MALE',
										email: 'qaebike@gmail.com',
										user_language: null,
										phone_prefix: null,
										phone_number: null,
										status: 'ACTIVE',
										accept_mail: false,
										accept_sms: false,
										accept_share: true,
										avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } ] );
									done();
								});
					});
			it('Invalid email or password."email" Status 401', (done) => {
						chai.request(urlBase)
							.post('/users/login')
							.set('content-type', 'application/x-www-form-urlencoded')
							.send({email:'qaebike+1aw@gmail.com', password: 'Ebike-app2018' })
							.end( function(err,res){
								expect(res).to.have.status('401');
								expect(res.body.status).to.equal(res.status);
								done();
							});
				});
			it('Invalid email or password."password" Status 401', (done) => {
						chai.request(urlBase)
							.post('/users/login')
							.set('content-type', 'application/x-www-form-urlencoded')
							.send({email:'qaebike+1@gmail.com', password: 'Ebike-app201999' })
							.end( function(err,res){
								expect(res).to.have.status('401');
								expect(res.body.status).to.equal(res.status);
								done();
							});
				});

			it('Possible response errors: The user have exceeded the number of failed login attempts.  Status 409', (done) => {
				let limitCall;
							//limitCall is the limit of call that one user can made wrong until have status 412 response.
				let endCall = new Promise(function(resolve, reject)	{
						for (limitCall = 0; limitCall < 10; limitCall++) {
								chai.request(urlBase)
									.post('/users/login')
									.set('content-type', 'application/x-www-form-urlencoded')
									.send({email:'qaebike+1@gmail.com', password: 'Ebike-app20202' })
									.end( function(err,res){
									//console.log(res.body.status)
								});
							}
							resolve();
						});

						endCall.then(()=>{
								chai.request(urlBase)
									.post('/users/login')
									.set('content-type', 'application/x-www-form-urlencoded')
									.send({email:'qaebike+1@gmail.com', password: 'Ebike-app20202' })
									.end( function(err,res){
										expect(res).to.have.status('409');
										expect(res.body.status).to.equal(res.status);
										done();
									});
							});

						});
					it('Possible response errors: User account pending to verify. Status 409', (done) => {
								chai.request(urlBase)
									.post('/users/login')
									.set('content-type', 'application/x-www-form-urlencoded')
									.send({email:'qaebike+8887@gmail.com', password: 'Ebike-app2018' })
									.end( function(err,res){
										expect(res).to.have.status('409');
										expect(res.body.status).to.equal(res.status);
										expect(res.body.code).to.equal('PENDING_VERIFY');
										done();
									});
						});
		});
describe('UC-2 getUsersMe:',()=>{
			it('Access with Token. Status 200', (done) => {
						chai.request(urlBase)
							.get('/users/me')
							.set('content-type', 'application/x-www-form-urlencoded')
							.set('Authorization', 'Bearer ' + Token)
							.set('accept-encoding', 'gzip, deflate')
							.set('Accept-Language', 'es_ES')
							.end( function(err,res){
								expect(res).to.have.status('200');
								expect(res.body.status).to.equal(res.status);
								expect(res.body.data).containSubset([{
									id: 8946,
					       	public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
					       	parent_users_id: null,
					       	firstname: 'qa',
					       	surname: 'YasminaProducción',
					       	birthdate: '2019-01-21 00:00:00',
					       	height: 159,
					       	weight: 56,
					       	id_facebook: null,
					       	gender: 'MALE',
					       	email: 'qaebike@gmail.com',
					       	user_language: null,
					       	phone_prefix: null,
					       	phone_number: null,
					       	status: 'ACTIVE',
					       	accept_mail: false,
					       	accept_sms: false,
					       	accept_share: true,
					       	avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } ] );
							done();
						});
			});
			it('Access with Cookie. Status 200', (done) => {
						chai.request(urlBase)
						.get('/users/me')
						.set('content-type', 'application/x-www-form-urlencoded')
						.set('Cookie', Cookies)
						.set('accept-encoding', 'gzip, deflate')
						.set('Accept-Language', 'es_ES')
						.end( function(err,res){
							expect(res).to.have.status('200');
							expect(res.body.status).to.equal(res.status);
							expect(res.body.data).containSubset([{
								id: 8946,
								public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
								parent_users_id: null,
								firstname: 'qa',
								surname: 'YasminaProducción',
								birthdate: '2019-01-21 00:00:00',
								height: 159,
								weight: 56,
								id_facebook: null,
								gender: 'MALE',
								email: 'qaebike@gmail.com',
								user_language: null,
								phone_prefix: null,
								phone_number: null,
								status: 'ACTIVE',
								accept_mail: false,
								accept_sms: false,
								accept_share: true,
								avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } ] );
							done();
						});
			});
				it('Invalid credentials. Status 401', (done) => {
							chai.request(urlBase)
								.get('/users/me')
								.set('content-type', 'application/x-www-form-urlencoded')
								.set('Authorization', 'Bearer ' + Token.concat('tokenInvalido'))
								.set('accept-encoding', 'gzip, deflate')
								.set('Accept-Language', 'es_ES')
								.end( function(err,res){
									expect(res).to.have.status('401');
									expect(res.body.status).to.equal(res.status);
									done();
								});
					});
	});
describe('UC-3 patchUsersMe:',()=>{
				it('Access with Token. Status 200', (done) => {
							chai.request(urlBase)
								.patch('/users/me')
								.set('content-type', 'application/x-www-form-urlencoded')
								.set('Authorization', 'Bearer ' + Token)
								.set('accept-encoding', 'gzip, deflate')
								.set('Accept-Language', 'es_ES')
								.send({
										firstname: 'qa',
										surname: 'YasminaProducción',
										password: 'Ebike-app2019',
										repeat_password: 'Ebike-app2019',
										birthdate: '2019-01-21 00:00:00',
										height: 159,
										weight: 56,
										id_facebook: null,
										gender: 'MALE',
										email: 'qaebike@gmail.com',
										user_language: null,
										phone_prefix: null,
										phone_number: null,
										accept_mail: false,
										accept_sms: false,
										accept_share: true,
										user_language: 'es_ES' })
								.end( function(err,res){
									expect(res).to.have.status('200');
									expect(res.body.status).to.equal(res.status);
									expect(res.body.data).containSubset([{
										id: 8946,
						       	public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
						       	parent_users_id: null,
						       	firstname: 'qa',
						       	surname: 'YasminaProducción',
						       	birthdate: '2019-01-21 00:00:00',
						       	height: 159,
						       	weight: 56,
						       	id_facebook: null,
						       	gender: 'MALE',
						       	email: 'qaebike@gmail.com',
						       	user_language: null,
						       	phone_prefix: null,
						       	phone_number: null,
						       	status: 'ACTIVE',
						       	accept_mail: false,
						       	accept_sms: false,
						       	accept_share: true,
						       	avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } ] );
								done();
							});
				});
				it('Access with Cookie. Status 200', (done) => {
							chai.request(urlBase)
							.get('/users/me')
							.set('content-type', 'application/x-www-form-urlencoded')
							.set('Cookie', Cookies)
							.set('accept-encoding', 'gzip, deflate')
							.set('Accept-Language', 'es_ES')
							.end( function(err,res){
								expect(res).to.have.status('200');
								expect(res.body.status).to.equal(res.status);
								expect(res.body.data).containSubset([{
									id: 8946,
									public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
									parent_users_id: null,
									firstname: 'qa',
									surname: 'YasminaProducción',
									birthdate: '2019-01-21 00:00:00',
									height: 159,
									weight: 56,
									id_facebook: null,
									gender: 'MALE',
									email: 'qaebike@gmail.com',
									user_language: null,
									phone_prefix: null,
									phone_number: null,
									status: 'ACTIVE',
									accept_mail: false,
									accept_sms: false,
									accept_share: true,
									avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } ] );
								done();
							});
				});
					it('Invalid credentials. Status 401', (done) => {
								chai.request(urlBase)
									.get('/users/me')
									.set('content-type', 'application/x-www-form-urlencoded')
									.set('Authorization', 'Bearer ' + Token.concat('tokenInvalido'))
									.set('accept-encoding', 'gzip, deflate')
									.set('Accept-Language', 'es_ES')
									.end( function(err,res){
										expect(res).to.have.status('401');
										expect(res.body.status).to.equal(res.status);
										done();
									});
						});
		});
