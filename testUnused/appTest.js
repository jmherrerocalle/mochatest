const assert = require('chai').assert;
const sayHello = require('../app').sayHello;
const addNumbers = require('../app').addNumbers;
//const app = require('../app');

describe('App', function(){
    describe('sayHello', function(){
      it('sayHello debería devolver hello', function(){
        let result = sayHello();
        /*El assert va a comprobar que el archivo a ejecutar devuelve
        el valor que estamos definiento, en este caso 'hello'*/
        assert.equal(result,'hello');
      });
      it('sayHello debería devolver un string', function(){
        let result = sayHello();
        /*El assert va a comprobar que el archivo a ejecutar devuelve
        el valor que estamos definiento, en este caso 'hello'*/
        assert.typeOf(result,'string');
      });
  });
  describe('addNumbers', function(){
      it('addMumbers should be above 5', function(){
        let result = addNumbers(5,1);
        /*El assert va comprobamos si tenemos valores superiores a 5 por ejemplo*/
        assert.isAbove(result,5);
      });
      it('addNumbers should be numbre type', function(){
          let result = addNumbers(5,5);
        /*El assert va a comprobar que el archivo a ejecutar devuelve
        el valor que estamos definiento, en este caso 'hello'*/
        assert.typeOf(result,'number');
      });
    it('addNumbers should be equal or less of 10', function(){
        let result = addNumbers(5,6);
      /*El assert va a comprobar que el archivo a ejecutar devuelve
      el valor que estamos definiento, en este caso 'hello'*/
      assert.isAtMost(result,10);
    });
  });
  //Another some commands:

						//expect(res).to.have.status('200');
						//res.body.data.id.equal('8926');
						//console.log(res.body);


});
