//paquetes que requerimos
const chaiAsPromised = require('chai-as-promised');    
let chai = require('chai');
let chaiHttp = require('chai-http');
let assertArrays = require('chai-arrays');
let chaiSubset = require('chai-subset');
let alert = require('alert-node');
const expect = require('chai').expect;
var mocha = require('mocha');
var describe = mocha.describe;
var it = mocha.it;
require('it-each')({ testPerIteration: true });
chai.use(chaiSubset);
chai.use(assertArrays);
chai.use(chaiHttp);
chai.use(chaiAsPromised);  
//definimos también la Url a la que vamos a llamar.
const urlBase= 'http://192.168.100.26:5001/api/v1';
var Cookies; //Cookie to auth
var Token; //Token to auth
//Array to define the number of iterations to test a wrong passwords.
var arrayCalls = [{'iteration':1, 'code':401}, {'iteration':2, 'code':401}, {'iteration':3, 'code':401}, {'iteration':4, 'code':401}, {'iteration':5, 'code':401}, {'iteration':6, 'code':409}]


describe('UC-1 postLogin Base:',()=>{
	it('Valid Email and password. Status 200', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike@gmail.com', password: 'Ebike-app' })
		.end( function(err,res){
			expect(res).to.have.status('200');
			expect(res.body.status).to.equal(res.status);
			expect('set-cookie', 'cookie=hey; Path=/', done);
			Cookies = res.headers['set-cookie'].pop().split(';')[0];
			Token = res.body.data.token;
			done();
		});
	});
	it('Expected response "User data": Status 200', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike+1@gmail.com', password: 'Ebike-app2018' })
		.end( function(err,res){
			//console.log(res.body.data)
			expect(res).to.have.status('200');
			expect(res.body.status).to.equal(res.status);
			expect(res.body.data).containSubset({
				id: 8926,
				public_id: '64adb1c8-786c-7300-ccc3-0126f705eea9',
				parent_users_id: null,
				firstname: 'José María',
				surname: 'Herrero',
				birthdate: '1988-02-16 00:00:00',
				height: 0,
				weight: 13.61,
				id_facebook: null,
				gender: 'MALE',
				email: 'qaebike+1@gmail.com',
				languages_id: 'en_US',
				phone_prefix: null,
				phone_number: null,
				status: 'ACTIVE',
				accept_mail: false,
				accept_sms: false,
				accept_share: true,
				preferences:{ 
					map_style: 'AUTOMATIC',
					object_type: 'COMPATIBLE',
					behavior_every: 5,
					map_headingMode: 'ROUTE',
					map_showCompass: false,
					health_sensitivity: 5,
					map_measuringSystem: 'METRIC',
					map_simulateNavigation: false,
					health_enableAutoAssist: false,
					health_heartRateMonitor: false,
					health_maximumHeartRate: 155,
					behavior_enableAutoPause: false,
					alert_nutrition_food_every: 30,
					map_voiceNavegationAdvices: false,
					alert_nutrition_drink_every: 30,
					alert_nutrition_food_enabled: false,
					alert_activity_distance_every: 5,
					alert_nutrition_drink_enabled: false,
					behavior_preferredOrientation: 'PORTRAIT',
					health_overrideRecommendedMHR: false,
					alert_activity_distance_enabled: false,
					alert_activity_noReturn_enabled: false,
					alert_weather_weatherAlert_every: 30,
					alert_activity_motorPower_enabled: false,
					behavior_enableSummaryAudioAdvice: false,
					alert_weather_weatherAlert_enabled: false,
					alert_activity_motorPower_percentage: 80,
					alert_heartRate_maxHeartRate_enabled: false,
					alert_heartRate_maxHeartRate_maximum: 155,
					behavior_enableContextualAudioAdvice: false },
				country_code: 'GB',
				brands: [],
				profiles: [],
				roles: [],
				country_name: 'United Kingdom',
				avatar: 'http://192.168.100.26:5001/assets/images/avatar.png'
				} );
			done();
			});
	});
	it('Invalid email or password."email" Status 401', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike+1aw@gmail.com', password: 'Ebike-app2018' })
		.end( function(err,res){
			expect(res).to.have.status('401');
			expect(res.body.status).to.equal(res.status);
			done();
		});
	});
	it('Invalid email or password."password" Status 401', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike+1@gmail.com', password: 'Ebike-app201999' })
		.end( function(err,res){
			expect(res).to.have.status('401');
			expect(res.body.status).to.equal(res.status);
			done();
		});
	});
	it.each(arrayCalls, 'Should return %d because we are testing five calls with wrong password. Number of iteration %d',['code', 'iteration'], function(element, next) { 
		if(element.iteration < arrayCalls.length){
			chai.request(urlBase)
			.post('/users/login')
			.set('content-type', 'application/x-www-form-urlencoded')
			.send({email:'qaebike+100@gmail.com', password: 'Ebike-app20202' })
			.end( function(err,res){
				expect(res).to.have.status('401');
				expect(res.body.status).to.equal(res.status);
				next()
			});
		} else {
			chai.request(urlBase)
			.post('/users/login')
			.set('content-type', 'application/x-www-form-urlencoded')
			.send({email:'qaebike+100@gmail.com', password: 'Ebike-app20202' })
			.end( function(err,res){
				expect(res).to.have.status('409');
				expect(res.body.status).to.equal(res.status);
				next()
			});
		}
	 });
	 it('Possible response errors: User account pending to verify. Status 409', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike+10@gmail.com', password: 'Ebike-app2018' })
		.end( function(err,res){
			expect(res).to.have.status('409');
			expect(res.body.status).to.equal(res.status);
			expect(res.body.code).to.equal('PENDING_VERIFY');
			done();
		});
	});
	it('Error: UNAVAILABLE_FOR_LEGAL_REASONS. One of the contents_id has been lost. Status 451', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike+1200@gmail.com', password: 'Ebike-app2018', contents_id: '1' })
		.end( function(err,res){
			expect(res).to.have.status('451');
			expect(res.body.status).to.equal(res.status);
			expect(res.body.code).to.equal('UNAVAILABLE_FOR_LEGAL_REASONS');
			done();
		});
	});
	it('Login updating the legal contents permissions. Status 200', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike+1@gmail.com', password: 'Ebike-app2018', contents_id: '1,2' })
		.end( function(err,res){
			expect(res).to.have.status('200');
			expect(res.body.status).to.equal(res.status);
			done();
		});
	});
});