//paquetes que requerimos
const chaiAsPromised = require('chai-as-promised');    
let chai = require('chai');
let chaiHttp = require('chai-http');
let assertArrays = require('chai-arrays');
let chaiSubset = require('chai-subset');
let alert = require('alert-node');
const expect = require('chai').expect;
var mocha = require('mocha');
var describe = mocha.describe;
var it = mocha.it;
require('it-each')({ testPerIteration: true });
chai.use(chaiSubset);
chai.use(assertArrays);
chai.use(chaiHttp);
chai.use(chaiAsPromised);  
//definimos también la Url a la que vamos a llamar.
const urlBase= 'http://192.168.100.26:5001/api/v1';
var Cookies; //Cookie to auth
var Token; //Token to auth

describe('Login, to establish Token and Cookies:',()=>{
	it('Valid Email and password. Status 200', (done) => {
		chai.request(urlBase)
		.post('/users/login')
		.set('content-type', 'application/x-www-form-urlencoded')
		.send({email:'qaebike@gmail.com', password: 'Ebike-app' })
		.end( function(err,res){
			expect(res).to.have.status('200');
			expect(res.body.status).to.equal(res.status);
			expect('set-cookie', 'cookie=hey; Path=/', done);
			Cookies = res.headers['set-cookie'].pop().split(';')[0];
			Token = res.body.data.token;
			done();
		});
	});
});
describe('UC-2 getUsersMe:',()=>{
	it('Access with Token. Status 200', (done) => {
		chai.request(urlBase)
		.get('/users/me')
		.set('content-type', 'application/x-www-form-urlencoded')
		.set('Authorization', 'Bearer ' + Token)
		.set('accept-encoding', 'gzip, deflate')
		.set('Accept-Language', 'es_ES')
		.end( function(err,res){
			expect(res).to.have.status('200');
			expect(res.body.status).to.equal(res.status);
			expect(res.body.data).containSubset({
				id: 8946,
				public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
				parent_users_id: null,
				firstname: 'qa',
				surname: 'YasminaProducción',
				birthdate: null,
				height: 159,
				weight: 56,
				id_facebook: null,
				gender: 'MALE',
				email: 'qaebike@gmail.com',
				phone_prefix: null,
				phone_number: null,
				status: 'ACTIVE',
				accept_mail: false,
				accept_sms: false,
				accept_share: true,
				avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } );
			done();
		});
	});
	it('Access with Cookie. Status 200', (done) => {
		chai.request(urlBase)
		.get('/users/me')
		.set('content-type', 'application/x-www-form-urlencoded')
		.set('Cookie', Cookies)
		.set('accept-encoding', 'gzip, deflate')
		.set('Accept-Language', 'es_ES')
		.end( function(err,res){
			expect(res).to.have.status('200');
			expect(res.body.status).to.equal(res.status);
			expect(res.body.data).containSubset({
				id: 8946,
				public_id: '21c9eca6-c1d9-dcf3-fe6c-44f7b320e3dd',
				parent_users_id: null,
				firstname: 'qa',
				surname: 'YasminaProducción',
				birthdate: null,
				height: 159,
				weight: 56,
				id_facebook: null,
				gender: 'MALE',
				email: 'qaebike@gmail.com',
				phone_prefix: null,
				phone_number: null,
				status: 'ACTIVE',
				accept_mail: false,
				accept_sms: false,
				accept_share: true,
				avatar: 'http://192.168.100.26:5001/assets/images/avatar.png' } );
			done();
		});
	});
	it('Invalid credentials. Status 401', (done) => {
		chai.request(urlBase)
		.get('/users/me')
		.set('content-type', 'application/x-www-form-urlencoded')
		.set('Authorization', 'Bearer ' + Token.concat('tokenInvalido'))
		.set('accept-encoding', 'gzip, deflate')
		.set('Accept-Language', 'es_ES')
		.end( function(err,res){
			expect(res).to.have.status('401');
			expect(res.body.status).to.equal(res.status);
			done();
		});
	});
});