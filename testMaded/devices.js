//requirement packets
//Para llamar a otro fichero y que este se ejecute facilitando funciones o variables.
//var login = require('../testMaded/login.js')
let chai = require('chai');
let chaiHttp = require('chai-http');
var chance = require('chance').Chance();
let assertArrays = require('chai-arrays');
let chaiSubset = require('chai-subset');
const expect = require('chai').expect;
//Instances
chai.use(chaiSubset);
chai.use(assertArrays);
chai.use(chaiHttp);
//url to call
const urlBase= 'http://192.168.100.26:5001/api/v1';

//Random variables to use
//Integer for uuid device
var uuidDevice= chance.integer({ min: 0, max: 999999999999999 });
//Android_ID for push_ID device
var android_ID= chance.android_id();



describe('Login Swagger:',()=>{
			it('Expected response "User data": Status 200', (done) => {
							chai.request(urlBase)
								.post('/users/login')
								.set('content-type', 'application/x-www-form-urlencoded')
                .set('Accept-Language', 'es_ES')
								.send({email:'qaebike@gmail.com', password: 'Ebike-app' })
								.end( function(err,res){
                  expect(res).to.have.status('200');
    							expect(res.body.status).to.equal(res.status);
                      token = res.body.data[0].token;
									done();
								});
				});
    });
describe('postDevices  Swagger:',()=>{
  		it('Insert device object. Status 200', (done) => {
  					chai.request(urlBase)
  						.post('/devices')
              .set('content-type', 'application/x-www-form-urlencoded')
              .set('Authorization', 'Bearer ' + token)
              .set('accept-encoding', 'gzip, deflate')
              .set('Accept-Language', 'es_ES')
  						.send({
                    uuid: uuidDevice,
                    device_os: 'ANDROID',
                    push_id: android_ID,
                    model: "V1",
                    brand: "Ebikemotion",
                    carrier: "IT",
                    user_agent: "ebikemotion 1.3.00.3764 (Android 6.0; V1; Ebikemotion; es)" })

  			       .end( function(err,res){
                    //console.log(res.body);
                    expect(res).to.have.status('200');
      							expect(res.body.status).to.equal(res.status);
      							done();
  						});
  			});
        it('Validation errors. Status 400', (done) => {
    					chai.request(urlBase)
    						.post('/devices')
                .set('content-type', 'application/x-www-form-urlencoded')
                .set('Authorization', 'Bearer ' + token)
                .set('accept-encoding', 'gzip, deflate')//todo revisar gcip
                .set('Accept-Language', 'es_ES')
    						.send({
                      uuid: uuidDevice,
                      device_os: 'ANDROID',
                      //push_id has maximun length in 250 characters.
                      push_id: "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789",
                      model: "",
                      brand: "Ebikemotion",
                      carrier: "IT",
                      user_agent: "ebikemotion 1.3.00.3764 (Android 6.0; V1; Ebikemotion; es)" })

    			       .end( function(err,res){
                      //console.log(res.body);
                      expect(res).to.have.status('400');
        							expect(res.body.status).to.equal(res.status);
        							done();
    						});
    			});
          it('UUID and DEVICE_OS is required. Status 400', (done) => {
                chai.request(urlBase)
                  .post('/devices')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + token)
                  .set('accept-encoding', 'gzip, deflate')//todo revisar gcip
                  .set('Accept-Language', 'es_ES')
                  .send({
                        uuid: uuidDevice,
                        device_os: '',
                        //push_id has maximun length in 250 characters.
                        push_id: android_ID,
                        model: "V",
                        brand: "Ebikemotion",
                        carrier: "IT",
                        user_agent: "ebikemotion 1.3.00.49939 (Android 6.0; V1; Ebikemotion; es)" })

                   .end( function(err,res){
                        //console.log(res.body);
                        expect(res).to.have.status('400');
                        expect(res.body.status).to.equal(res.status);
                        done();
                  });
            });
          it('Invalid credentials. Status 401', (done) => {
      					chai.request(urlBase)
      						.post('/devices')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + token.concat('tokenInvalido'))
                  .set('accept-encoding', 'gzip, deflate')
                  .set('Accept-Language', 'es_ES')
      						.send({
                        uuid: uuidDevice,
                        device_os: 'ANDROID',
                        push_id: android_ID,
                        model: "V1",
                        brand: "Ebikemotion",
                        carrier: "IT",
                        user_agent: "ebikemotion 1.3.00.3764 (Android 6.0; V1; Ebikemotion; es)" })

      			       .end( function(err,res){
                        //console.log(res.body);
                        expect(res).to.have.status('401');
          							expect(res.body.status).to.equal(res.status);
          							done();
      						});
      			});
});
