let chai = require('chai');
let chaiHttp = require('chai-http');
let assertArrays = require('chai-arrays');
let chaiSubset = require('chai-subset');
var chance = require('chance').Chance();
const expect = require('chai').expect;
chai.use(chaiSubset);
chai.use(assertArrays);
chai.use(chaiHttp);
//URL to call.
const urlBase= 'http://192.168.100.26:5001/api/v1';
var Cookies; //Cookie to auth
var Token; //Token to auth


//We need diferents password to check this functionality.
//We can not use the last 5 passwords used.
var PasswordBase = 'Ebike-app2018'; //Begin and end password, complete the cicle.
var PasswordRandomFirst;
var PasswordRandomSecond;
var PasswordRandomThree;
var PasswordRandomFour;
var PasswordRandomFive;

PasswordRandomFirst= PasswordBase.concat('1');
PasswordRandomSecond= PasswordBase.concat('2');
PasswordRandomThree= PasswordBase.concat('3');
PasswordRandomFour= PasswordBase.concat('4');
PasswordRandomFive= PasswordBase.concat('5');
console.log('************************************')
console.log('*                                  *')
console.log('*Take care with the passwords order*')
console.log('*                                  *')
console.log('************************************')

describe('postLogin Base:',()=>{
			it('Expected response "User data": Status 200', (done) => {
							chai.request(urlBase)
								.post('/users/login')
								.set('content-type', 'application/x-www-form-urlencoded')
								.send({email:'qaebike+100@gmail.com', password: PasswordBase })
								.end( function(err,res){
									expect(res).to.have.status('200');
									expect(res.body.status).to.equal(res.status);
                  expect('set-cookie', 'cookie=hey; Path=/', done);
                  Cookies = res.headers['set-cookie'].pop().split(';')[0];
                  Token = res.body.data[0].token;
									done();
								});
					});
});

describe('UC-1 patchUsersMePassword:',()=>{
       it('1º Change password correctly with Token. Status 200', (done) => {
						chai.request(urlBase)
								.patch('/users/me/password')
								.set('content-type', 'application/x-www-form-urlencoded')
								.set('Authorization', 'Bearer ' + Token)
								.set('accept-encoding', 'gzip, deflate')
								.set('Accept-Language', 'es_ES')
								.send({
                    old_password: PasswordBase,
										password: PasswordRandomFirst,
										repeat_password: PasswordRandomFirst })
								.end( function(err,res){
									expect(res).to.have.status('200');
									expect(res.body.status).to.equal(res.status);
									expect(res.body.data).containSubset([{
						       	firstname: 'qa',
						       	surname: 'ebike100',
						       	email: 'qaebike+100@gmail.com'} ] );
								 done();
							});
				});

			it('2º Change password correctly with Cookie. Status 200', (done) => {
							chai.request(urlBase)
							.patch('/users/me/password')
							.set('content-type', 'application/x-www-form-urlencoded')
							.set('Cookie', Cookies)
							.set('accept-encoding', 'gzip, deflate')
							.set('Accept-Language', 'es_ES')
              .send({
                  old_password: PasswordRandomFirst,
                  password: PasswordRandomSecond,
                  repeat_password:PasswordRandomSecond })
              .end( function(err,res){
                //console.log(res.body)
                expect(res).to.have.status('200');
                expect(res.body.status).to.equal(res.status);
                expect(res.body.data).containSubset([{
                  firstname: 'qa',
                  surname: 'ebike100',
                  email: 'qaebike+100@gmail.com'} ] );
              done();
            });
				});
      it('Error: Invalid Credentials-Cookie. Status 401', (done) => {
  							chai.request(urlBase)
  							.patch('/users/me/password')
  							.set('content-type', 'application/x-www-form-urlencoded')
  							.set('Cookie', Cookies.concat('wrong'))
  							.set('accept-encoding', 'gzip, deflate')
  							.set('Accept-Language', 'es_ES')
                .send({
                    old_password: PasswordRandomFirst,
                    password: PasswordRandomSecond,
                    repeat_password:PasswordRandomSecond })
                .end( function(err,res){
                  //console.log(res.body)
                  expect(res).to.have.status('401');
                  expect(res.body.status).to.equal(res.status);
                done();
              });
  				});
      it('Error: Invalid Credentials-Token. Status 401', (done) => {
      							chai.request(urlBase)
      							.patch('/users/me/password')
      							.set('content-type', 'application/x-www-form-urlencoded')
      							.set('Authorization', 'Bearer ' + Token.concat('wrong'))
      							.set('accept-encoding', 'gzip, deflate')
      							.set('Accept-Language', 'es_ES')
                    .send({
                        old_password: PasswordRandomFirst,
                        password: PasswordRandomSecond,
                        repeat_password:PasswordRandomSecond })
                    .end( function(err,res){
                      //console.log(res.body)
                      expect(res).to.have.status('401');
                      expect(res.body.status).to.equal(res.status);
                    done();
                  });
      				});
      it('3º Change password correctly with Token. Status 200', (done) => {
              chai.request(urlBase)
              .patch('/users/me/password')
              .set('content-type', 'application/x-www-form-urlencoded')
              .set('Authorization', 'Bearer ' + Token)
              .set('accept-encoding', 'gzip, deflate')
              .set('Accept-Language', 'es_ES')
              .send({
                  old_password: PasswordRandomSecond,
                  password: PasswordRandomThree,
                  repeat_password:PasswordRandomThree })
              .end( function(err,res){
                //console.log(res.body)
                expect(res).to.have.status('200');
                expect(res.body.status).to.equal(res.status);
                expect(res.body.data).containSubset([{
                  firstname: 'qa',
                  surname: 'ebike100',
                  email: 'qaebike+100@gmail.com'} ] );
              done();
            });
        });
    it('4º Change password correctly with Token. Status 200', (done) => {
              chai.request(urlBase)
              .patch('/users/me/password')
              .set('content-type', 'application/x-www-form-urlencoded')
              .set('Authorization', 'Bearer ' + Token)
              .set('accept-encoding', 'gzip, deflate')
              .set('Accept-Language', 'es_ES')
              .send({
                  old_password: PasswordRandomThree,
                  password: PasswordRandomFour,
                  repeat_password:PasswordRandomFour })
              .end( function(err,res){
                //console.log(res.body)
                expect(res).to.have.status('200');
                expect(res.body.status).to.equal(res.status);
                expect(res.body.data).containSubset([{
                  firstname: 'qa',
                  surname: 'ebike100',
                  email: 'qaebike+100@gmail.com'} ] );
              done();
            });
        });
      it('Error: Can not repeat last 5 passwords used. Status 400', (done) => {
              chai.request(urlBase)
              .patch('/users/me/password')
              .set('content-type', 'application/x-www-form-urlencoded')
              .set('Authorization', 'Bearer ' + Token)
              .set('accept-encoding', 'gzip, deflate')
              .set('Accept-Language', 'es_ES')
              .send({
                  old_password: PasswordRandomFour,
                  password: PasswordBase,
                  repeat_password:PasswordBase })
              .end( function(err,res){
                //console.log(res.body)
                expect(res).to.have.status('400');
                expect(res.body.status).to.equal(res.status);
              done();
            });
        });
      it('5º Change password correctly with Cookie. Status 200', (done) => {
              chai.request(urlBase)
              .patch('/users/me/password')
              .set('content-type', 'application/x-www-form-urlencoded')
              .set('Authorization', 'Bearer ' + Token)
              .set('accept-encoding', 'gzip, deflate')
              .set('Accept-Language', 'es_ES')
              .send({
                  old_password: PasswordRandomFour,
                  password: PasswordRandomFive,
                  repeat_password:PasswordRandomFive })
              .end( function(err,res){
                //console.log(res.body)
                expect(res).to.have.status('200');
                expect(res.body.status).to.equal(res.status);
                expect(res.body.data).containSubset([{
                  firstname: 'qa',
                  surname: 'ebike100',
                  email: 'qaebike+100@gmail.com'} ] );
              done();
            });
        });
      it('6º After of 5 different passwords, we can insert the original password again. Status 200', (done) => {
              chai.request(urlBase)
              .patch('/users/me/password')
              .set('content-type', 'application/x-www-form-urlencoded')
              .set('Authorization', 'Bearer ' + Token)
              .set('accept-encoding', 'gzip, deflate')
              .set('Accept-Language', 'es_ES')
              .send({
                  old_password: PasswordRandomFive,
                  password: PasswordBase,
                  repeat_password:PasswordBase })
              .end( function(err,res){
                //console.log(res.body)
                expect(res).to.have.status('200');
                expect(res.body.status).to.equal(res.status);
                expect(res.body.data).containSubset([{
                  firstname: 'qa',
                  surname: 'ebike100',
                  email: 'qaebike+100@gmail.com'} ] );
              done();
            });
        });
    });

  describe('UC-2 patchUsersMePassword:',()=>{
           it('1º Password: Empty String - Error expected: La contraseña es obligatoria. Status 400', (done) => {
    						chai.request(urlBase)
    								.patch('/users/me/password')
    								.set('content-type', 'application/x-www-form-urlencoded')
    								.set('Authorization', 'Bearer ' + Token)
    								.set('accept-encoding', 'gzip, deflate')
    								.set('Accept-Language', 'es_ES')
    								.send({
                        old_password: PasswordBase,
    										password: '',
    										repeat_password: ''})
    								.end( function(err,res){
    									expect(res).to.have.status('400');
    									expect(res.body.status).to.equal(res.status);
    								 done();
    							});
    				});

    			it('2º Password: Ebike-a - Error expected: ¡Al menos {0} caracteres por favor! 8. Status 400', (done) => {
    							chai.request(urlBase)
    							.patch('/users/me/password')
    							.set('content-type', 'application/x-www-form-urlencoded')
    							.set('Authorization', 'Bearer ' + Token)
    							.set('accept-encoding', 'gzip, deflate')
    							.set('Accept-Language', 'es_ES')
                  .send({
                      old_password: PasswordBase,
                      password: 'Ebike-a',
                      repeat_password:'Ebike-a' })
                  .end( function(err,res){
                    //console.log(res.body)
                    expect(res).to.have.status('400');
                    expect(res.body.status).to.equal(res.status);
                  done();
                });
    				});
          it('3º Password: 1E1239567 - Error expected: ¡Al menos {0} caracter en minúsculas por favor! 1. Status 400', (done) => {
                  chai.request(urlBase)
                  .patch('/users/me/password')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + Token)
                  .set('accept-encoding', 'gzip, deflate')
                  .set('Accept-Language', 'es_ES')
                  .send({
                      old_password: PasswordBase,
                      password: '1E1239567',
                      repeat_password: '1E1239567' })
                  .end( function(err,res){
                    //console.log(res.body)
                    expect(res).to.have.status('400');
                    expect(res.body.status).to.equal(res.status);
                  done();
                });
            });
        it('4º Password: Ebike--app21888 - Error expected: ¡No más de {0} caracteres o números repetidos de forma consecutiva por favor! 2. Status 400', (done) => {
                  chai.request(urlBase)
                  .patch('/users/me/password')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + Token)
                  .set('accept-encoding', 'gzip, deflate')
                  .set('Accept-Language', 'es_ES')
                  .send({
                      old_password: PasswordBase,
                      password: 'Ebike--app21888',
                      repeat_password: 'Ebike--app21888' })
                  .end( function(err,res){
                    //console.log(res.body)
                    expect(res).to.have.status('400');
                    expect(res.body.status).to.equal(res.status);
                  done();
                });
            });
          it('5º Password: Ebike---app218 - Error expected: ¡No más de {0} caracteres o números repetidos de forma consecutiva por favor! 2. Status 400', (done) => {
                  chai.request(urlBase)
                  .patch('/users/me/password')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + Token)
                  .set('accept-encoding', 'gzip, deflate')
                  .set('Accept-Language', 'es_ES')
                  .send({
                      old_password: PasswordBase,
                      password: 'Ebike---app218',
                      repeat_password: 'Ebike---app218' })
                  .end( function(err,res){
                    //console.log(res.body)
                    expect(res).to.have.status('400');
                    expect(res.body.status).to.equal(res.status);
                  done();
                });
            });
          it('6º Password: Ebike--appp218 - Error expected: ¡No más de {0} caracteres o números repetidos de forma consecutiva por favor! 2. Status 400', (done) => {
                  chai.request(urlBase)
                  .patch('/users/me/password')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + Token)
                  .set('accept-encoding', 'gzip, deflate')
                  .set('Accept-Language', 'es_ES')
                  .send({
                      old_password: PasswordBase,
                      password: 'Ebike--appp218',
                      repeat_password: 'Ebike--appp218'})
                  .end( function(err,res){
                    //console.log(res.body)
                    expect(res).to.have.status('400');
                    expect(res.body.status).to.equal(res.status);
                  done();
                });
            });
          it('7º Password: Ebikeapp - Error expected: ¡Al menos {0} número o caracter especial por favor! 1. Status 400', (done) => {
                  chai.request(urlBase)
                  .patch('/users/me/password')
                  .set('content-type', 'application/x-www-form-urlencoded')
                  .set('Authorization', 'Bearer ' + Token)
                  .set('accept-encoding', 'gzip, deflate')
                  .set('Accept-Language', 'es_ES')
                  .send({
                      old_password: PasswordBase,
                      password: 'Ebikeapp',
                      repeat_password: 'Ebikeapp' })
                  .end( function(err,res){
                    //console.log(res.body)
                    expect(res).to.have.status('400');
                    expect(res.body.status).to.equal(res.status);
                  done();
                });
            });
            it('8º Password: bike-app2018 - Error expected: ¡Al menos {0} caracter en mayúsculas por favor! 1. Status 400', (done) => {
                    chai.request(urlBase)
                    .patch('/users/me/password')
                    .set('content-type', 'application/x-www-form-urlencoded')
                    .set('Authorization', 'Bearer ' + Token)
                    .set('accept-encoding', 'gzip, deflate')
                    .set('Accept-Language', 'es_ES')
                    .send({
                        old_password: PasswordBase,
                        password: 'bike-app2018',
                        repeat_password: 'bike-app2018' })
                    .end( function(err,res){
                      //console.log(res.body)
                      expect(res).to.have.status('400');
                      expect(res.body.status).to.equal(res.status);
                    done();
                  });
              });
        });

        describe('UC-3 patchUsersMePassword:',()=>{
                 it('Wrong Old_Password. Error expected: La contraseña es obligatoria. Status 400', (done) => {
                      chai.request(urlBase)
                          .patch('/users/me/password')
                          .set('content-type', 'application/x-www-form-urlencoded')
                          .set('Authorization', 'Bearer ' + Token)
                          .set('accept-encoding', 'gzip, deflate')
                          .set('Accept-Language', 'es_ES')
                          .send({
                              old_password: 'Ebike-app2018OldWrong',
                              password: 'Ebike-app2018',
                              repeat_password: 'Ebike-app2018'})
                          .end( function(err,res){
                            expect(res).to.have.status('400');
                            expect(res.body.status).to.equal(res.status);
                           done();
                        });
                  });
        });
